package data;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class getDataExcel {
	//close file, close inputstream
	static Logger logger = Logger.getLogger(getDataExcel.class);
	
	public static String[][] getData(String fileInputPath) {
		List<Map> list = new ArrayList();
		String[][] dataArray = null;
		try {
			FileInputStream ipstr = new FileInputStream(new File(fileInputPath));
			HSSFWorkbook wb = new HSSFWorkbook(ipstr);
			HSSFSheet ws = wb.getSheetAt(0);
			int rowNum = ws.getLastRowNum() + 1;
			int colNum = ws.getRow(0).getLastCellNum();
			dataArray = new String[rowNum][colNum];
			for (int i = 0; i < rowNum; i++) {
				HSSFRow row = ws.getRow(i);
				// reset new memmory for each map
				Map map = new HashMap<String, String>();
				
				map.put("tcID", row.getCell(0).getStringCellValue());
				map.put("name", row.getCell(1).getStringCellValue());
				map.put("email", row.getCell(2).getStringCellValue());
				map.put("password", row.getCell(3).getStringCellValue());
				map.put("passwordConfirmation", row.getCell(4).getStringCellValue());
				map.put("notes", row.getCell(5).getStringCellValue());

				list.add(map);
			}
			ipstr.close();
		} catch (Exception e) {
			logger.error(e);
		}

		for (int i = 1; i < list.size(); i++) {
			Map<String, String> x = list.get(i);
			String tcID = x.get("tcID");
			String name = x.get("name");
			String email = x.get("email");
			String password = x.get("password");
			String passwordConfirmation = x.get("passwordConfirmation");
			String notes = x.get("notes");
			
			//skip tile of colums
			dataArray[i-1][0] = tcID;
			dataArray[i-1][1] = name;
			dataArray[i-1][2] = email;
			dataArray[i-1][3] = password;
			dataArray[i-1][4] = passwordConfirmation;
			dataArray[i-1][5] = notes;
		}

		return dataArray;
	}
}
