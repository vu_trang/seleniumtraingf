package data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import test.LoginTest;

public class writeDataExcel {
	static Logger logger = Logger.getLogger(writeDataExcel.class);
	public static void witeData(String fileTempale, String fileResult,List outputList) {
		try (InputStream is = new FileInputStream(fileTempale)) {
			try (OutputStream os = new FileOutputStream(fileResult)) {
				Context context = new Context();
				context.putVar("results", outputList);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			} catch (Exception e) {
				logger.error(e);
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
}
