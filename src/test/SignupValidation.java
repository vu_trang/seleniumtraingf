package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.getDataExcel;
import data.writeDataExcel;

public class SignupValidation {
	WebDriver driver = null;
	static Logger logger = Logger.getLogger(LoginTest.class);
	static String fileInputFailPath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/dataSignupFail.xls";
	static String fileInputSucessPath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/dataSignupSuccess.xls";
	static String fileTemplatePath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/result-template.xls";
	static String fileOutputPath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/result-signup.xls";
	String GECKODRIVER_PATH = "C:/Users/vu.thi.trang/Downloads/geckodriver/geckodriver.exe";
	String APP_URL = "https://selenium-training.herokuapp.com";

	@Test(dataProvider = "signupDataFail", priority = 0, enabled = false)
	public void testSignupFail(String dataID, String name, String email, String password, String passwordConfirmation,
			String notes) {
		// add ID data
		signup(dataID, name, email, password, passwordConfirmation, notes);

		boolean resultTC = valiateSignupFail();
		// assertTrue(resultTC);

		Map<String, Object> testResult = new HashMap<String, Object>();
		testResult.put("testsuite", "TS-01 SIGN UP");
		testResult.put("testcase", "Signup Test");
		testResult.put("testdata", dataID + " , " + email);
		testResult.put("testresult", resultTC);
		List results = new ArrayList();
		results.add(testResult);
		writeDataExcel.witeData(fileTemplatePath, fileOutputPath, results);
		logger.info("Data ID" + dataID + "...notes..." + notes + "..... Result... " + resultTC);
	}

	public void signup(String dataID, String name, String email, String password, String passwordConfirmation,
			String notes) {
		driver.navigate().to(APP_URL);
		driver.findElement(By.linkText("Sign up now!")).click();

		new WebDriverWait(driver, 10000)
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("form[id='new_user']")));
		WebElement signupForm = driver.findElement(By.cssSelector("form[id='new_user']"));
		signupForm.findElement(By.name("user[name]")).sendKeys(name);
		signupForm.findElement(By.name("user[email]")).sendKeys(email);
		signupForm.findElement(By.name("user[password]")).sendKeys(password);
		signupForm.findElement(By.name("user[password_confirmation]")).sendKeys(passwordConfirmation);
		new WebDriverWait(driver, 10000)
				.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[type='submit']")));
		signupForm.findElement(By.cssSelector("input[type='submit']")).submit();
	}

	public boolean valiateSignupFail() {
		WebElement errorMsg = driver.findElement(By.id("error_explanation"));
		assertTrue(errorMsg.isDisplayed());
		List<WebElement> errorList = new ArrayList<WebElement>();
		errorList = errorMsg.findElements(By.tagName("ul li"));
		for (int i = 0; i < errorList.size(); i++) {
			String error = errorList.get(i).getText();
			logger.info("message error ..." + error);
		}

		return errorMsg.isDisplayed();
	}

	@Test(dataProvider = "signupDataSuccess",enabled=true)
	public void testSignupSuccess(String dataID, String name, String email, String password,
			String passwordConfirmation, String notes) {

		signup(dataID, name, email, password, passwordConfirmation, notes);

		boolean resultTC = valiateSignupSuccess();
		logger.info("Data ID" + dataID + "...notes..." + notes + "..... Result... " + resultTC);
	}

	public boolean valiateSignupSuccess() {
		return driver.getPageSource().contains("Welcome to");
	}

	@DataProvider(name = "signupDataFail")
	public String[][] testDataFail() throws Exception {
		String dataArray[][] = getDataExcel.getData(fileInputFailPath);
		return dataArray;
	}

	@DataProvider(name = "signupDataSuccess")
	public String[][] testDataSuccess() throws Exception {
		return getDataExcel.getData(fileInputSucessPath);
	}

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", GECKODRIVER_PATH);
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}
}
