package test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.getDataExcel;
import data.writeDataExcel;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTest {
	WebDriver driver = null;
	static Logger logger = Logger.getLogger(LoginTest.class);
	static String fileInputPath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/data.xls";
	static String fileTemplatePath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/result-template.xls";
	static String fileOutputPath = "C:/Users/vu.thi.trang/workspace/SeleniumTraining/src/data/result-out.xls";
	String GECKODRIVER_PATH = "C:\\Users\\vu.thi.trang\\Downloads\\geckodriver\\geckodriver.exe";
	String APP_URL = "https://selenium-training.herokuapp.com";

	@DataProvider(name = "loginData")
	public String[][] testData() throws Exception {
		// String dataArray[][] =
		return getDataExcel.getData(fileInputPath);
		// return dataArray;
	}

	@Test(dataProvider = "loginData")
	public void testLogin(String email, String password) {
		boolean isPassed = false;

		login(email, password);

		Map<String,Object> testResult = new HashMap<String, Object>();
		testResult.put("testsuite", "TS-01 Login");
		testResult.put("testcase", "Login Test");
		testResult.put("testdata", email + " , " + password);
		testResult.put("testresult", isPassed);
		List results = new ArrayList();
		results.add(testResult);
		writeDataExcel.witeData(fileTemplatePath, fileOutputPath, results);
	}

	public void login(String email, String password) {
		driver.findElement(By.linkText("Log in")).click();

		new WebDriverWait(driver, 10000)
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("form[action='/login']")));
		WebElement logInFrom = driver.findElement(By.cssSelector("form[action='/login']"));
		logInFrom.findElement(By.name("session[email]")).sendKeys(email);
		logInFrom.findElement(By.name("session[password]")).sendKeys(password);
		new WebDriverWait(driver, 10000)
				.until(ExpectedConditions.elementToBeClickable(By.cssSelector("form[action='/login']")));
		logInFrom.findElement(By.cssSelector("input[type='submit']")).submit();
	}

	@BeforeTest
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver", GECKODRIVER_PATH);
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.navigate().to(APP_URL);
	}

	@AfterTest
	public void closebrowser() {
		driver.quit();
	}
}
